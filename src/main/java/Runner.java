import utils.compile.dtos.Longitude;
import utils.compile.dtos.Person;
import utils.compiler.factory.SerializerFactory;
import utils.compiler.ifaces.ISerializer;
import utils.compiler.impl.json.JsonSchemaToJavaClassToJava;

import java.util.Locale;
import java.util.ResourceBundle;

public class Runner {
    public static void main(String[] args) {

        // 1. Create java-file
        JsonSchemaToJavaClassToJava.compileToJava("src\\main\\java\\utils\\resources\\test.json");

        // 2. Serialize
        ResourceBundle bundle = ResourceBundle.getBundle("in", Locale.ENGLISH);
        Person person = new Person("Name", "L_Name", 110);
        Longitude longitude = new Longitude(1, 2.2212);
        ISerializer serializer = SerializerFactory.getClassFromFactory(bundle);
        serializer.compileToJson(person);
        serializer.compileToJson(longitude);

    }
}
