package utils.compile.dtos;

public class Longitude{

	private Number latitude;
	private Number longitude;

	public Longitude(Number latitude, Number longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

}