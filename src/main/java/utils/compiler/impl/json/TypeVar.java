package utils.compiler.impl.json;

public enum TypeVar {

    STRING("String"), INTEGER("int"), NUMBER("Number");
    private String nameVar;

    TypeVar(String name) {
        this.nameVar = name;
    }

    public String getNameVar() {
        return nameVar;
    }

    public void setNameVar(String nameVar) {
        this.nameVar = nameVar;
    }
}
