package utils.compiler.impl.json;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonSchemaToJavaClassToJava {

    public static void compileToJava(String pathToJsonSchema) {
        try (Scanner reader = new Scanner(new FileReader(pathToJsonSchema))) {
            StringBuilder builder = new StringBuilder();
            while (reader.hasNext()) {
                builder.append(reader.nextLine().trim());
            }
            String jsonOneLine = builder.toString();
            Pattern pattern = Pattern.compile("properties");
            String[] tags = pattern.split(jsonOneLine);
            //Parse title for class
            String titleClass = findTitle(tags[0]);
            //Parse fields
            String[] prop = tags[1].split("},");
            HashMap<String, String> fields = new HashMap<>(generateFieldsAndTypes(prop));
            //Generate java-class to file
            createJavaClass(titleClass, fields);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void createJavaClass(String title, HashMap<String, String> fields) throws IOException {
        FileWriter writer = new FileWriter("src/main/java/utils/compile/dtos/" + title + ".java", false);
        writer.write("package utils.compile.dtos;\n\npublic class " + title + "{\n\n");
        //wRITE FIELDS
        StringBuilder typeAndFieldName = new StringBuilder();
        for (Map.Entry entry : fields.entrySet()) {
            TypeVar type = TypeVar.valueOf((String) entry.getValue());
            String strFields = type.getNameVar() + " " + entry.getKey();
            writer.write("\tprivate " + strFields + ";\n");
            typeAndFieldName.append(strFields).append(", ");
        }
        //Write constructor
        writer.write("\n\tpublic " + title + "(" + typeAndFieldName.replace(
                typeAndFieldName.length() - 2,
                typeAndFieldName.length(),
                ") {\n"));
        for (Map.Entry entry : fields.entrySet()) {
            String strField = "" + entry.getKey();
            writer.write("\t\tthis." + strField + " = " + strField + ";\n");
        }
        writer.write("\t}\n");

        //end class
        writer.write("\n}");
        writer.flush();
    }

    private static String findTitle(String json) {
        String title = "";
        String[] strings = json.split("title");
        Pattern pattern = Pattern.compile("[A-z0-9_-]+");
        Matcher matcher = pattern.matcher(strings[1]);
        if (matcher.find()) {
            title = matcher.group();
        }
        return title;
    }

    private static Map<String, String> generateFieldsAndTypes(String[] json) {
        int row = json.length;
        int col = 2;
        String[][] fields = new String[row][col];

        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < row; i++) {
            String[] rowJson = json[i].split("type");
            for (int j = 0; j < col; j++) {
                fields[i][j] = rowJson[j];
                Pattern pattern = Pattern.compile("[A-z0-9_-]+");
                Matcher matcher = pattern.matcher(fields[i][j]);
                if (matcher.find()) {
                    fields[i][j] = matcher.group();
                }
            }
            map.put(fields[i][0], fields[i][1].toUpperCase());
        }
        return map;
    }
}
