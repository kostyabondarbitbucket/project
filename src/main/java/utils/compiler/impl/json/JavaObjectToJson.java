package utils.compiler.impl.json;

import utils.compiler.ifaces.ISerializer;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

public class JavaObjectToJson implements ISerializer {

    public void compileToJson(Object o){
        try {
            FileWriter writer = new FileWriter("src/main/java/utils/compile/json/" + o.getClass().getSimpleName() + ".json", false);
            writer.write("{\n");
            Field[] fields = o.getClass().getDeclaredFields();
            int count = fields.length;
            for (Field field : fields) {
                field.setAccessible(true);
                writer.write("\"" + field.getName() + "\": ");
                String strArg = field.getType().getSimpleName().equals("String")
                        ? "\"" + field.get(o) + "\""
                        : field.get(o).toString();
                writer.write(strArg);
                writer.write(count > 1 ? ",\n" : "\n");
                count--;
            }
            writer.write("}");
            writer.flush();

        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
