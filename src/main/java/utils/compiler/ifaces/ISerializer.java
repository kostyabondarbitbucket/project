package utils.compiler.ifaces;

public interface ISerializer {
    void compileToJson(Object o);
}
