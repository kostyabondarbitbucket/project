package utils.compiler.factory;

import utils.compiler.Constants;
import utils.compiler.ifaces.ISerializer;
import utils.compiler.impl.json.JavaObjectToJson;
import java.util.ResourceBundle;

public class SerializerFactory {

    private enum Source {
        JSON {
            @Override
            ISerializer getImpl(ResourceBundle bundle) {
                return new JavaObjectToJson();
            }
        };

        abstract ISerializer getImpl(ResourceBundle bundle);
    }

    public static ISerializer getClassFromFactory(ResourceBundle bundle) {
        String src = bundle.getString(Constants.SOURCE);
        Source source = Source.valueOf(src.toUpperCase());
        return source.getImpl(bundle);
    }
}
